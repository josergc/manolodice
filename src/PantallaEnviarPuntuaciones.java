/**
	Nombre del proyecto:
	Comunes

	Descripci�n de la clase:
	En esta pantalla se env�a autom�ticamente la tabla de puntuaciones
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Licencia:
	GNU/GPL versi�n 2 o superior
	
	Hist�rico:
	1.0 - 26/11/2006
*/

import java.io.*;
import javax.microedition.io.*;
import javax.microedition.lcdui.*;
import javax.wireless.messaging.*;

public class PantallaEnviarPuntuaciones extends Form implements Runnable {
	protected int idJuego;
	protected Puntuaciones puntuaciones;
	protected AvisoPuntuacionesEnviadas apv;
	protected AvisoErrorEnviandoPuntuaciones aeep;

	public PantallaEnviarPuntuaciones(AvisoPuntuacionesEnviadas apv, AvisoErrorEnviandoPuntuaciones aeep, int idJuego, Puntuaciones puntuaciones) {
		super("Enviando puntuaciones...");
		
		this.apv = apv;
		this.aeep = aeep;
		this.idJuego = idJuego;
		this.puntuaciones = puntuaciones;
	}
	
	public void run() {
		// Env�a el mensaje
		synchronized (this) {
			HttpConnection conexion = null;
			InputStream is = null;
			OutputStream os = null;
			StringItem si;
			int codigoRespuesta;
			for (int i = 0; i < puntuaciones.devNumRegistros(); i++)
				if (!puntuaciones.estaEnviada(i)) 
					try {
						append(si = new StringItem("Env�o l�nea",Integer.toString(i)));
						append(si = new StringItem("Abriendo conexi�n...",null));
						conexion = (HttpConnection)Connector.open(
							"http://josergc.up.md/nueva_puntuacion.php"
							);
						si.setText("�hecho!");
						append(si = new StringItem("Enviando datos...",null));
						conexion.setRequestMethod(HttpConnection.POST);
						conexion.setRequestProperty("User-Agent","MIDP-1.0 CLDC-1.0");
						conexion.setRequestProperty("Accept","text/html,text/plain");
						conexion.setRequestProperty("Accept-Language","es-ES");
						conexion.setRequestProperty("Keep-Alive","300");
						conexion.setRequestProperty("Connection","keep-alive");
						conexion.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
						os = conexion.openOutputStream();
						os.write((
							"idJuego=" + idJuego + 
							"&puntos=" + puntuaciones.devPuntos(i) + 
							"&nivel=" + puntuaciones.devNivel(i) + 
							"&nombre=" + puntuaciones.devNombre(i)
							).getBytes());
						si.setText("�hecho!");
						append(si = new StringItem("Recibiendo respuesta...",null));
						codigoRespuesta = conexion.getResponseCode();
						if (codigoRespuesta != HttpConnection.HTTP_OK) {
							aeep.errorEnviandoPuntuaciones("�Error! " + codigoRespuesta);
							i = puntuaciones.devNumRegistros();
						}
						si.setText("�hecho!");
						puntuaciones.ponEnviado(i);
					} catch(IOException e) {
						aeep.errorEnviandoPuntuaciones(
							"Error de entrada/salida: No se ha podido realizar la operaci�n.\n" + e.toString() + "\nP�ngase en contacto conmigo por el e-mail josergc@lycos.es o por la p�gina web www.josergc.tk"
							);
						i = puntuaciones.devNumRegistros();
					} catch(ClassCastException e) {
						aeep.errorEnviandoPuntuaciones(
							"No se puede realizar la acci�n: Este dispositivo m�vil no est� capacitado para hacer conexiones HTTP.\nP�ngase en contacto conmigo por el e-mail josergc@lycos.es o por la p�gina web www.josergc.tk"
							);
						i = puntuaciones.devNumRegistros();
					} finally {
						try { if (is != null) is.close(); } catch(IOException e) { }
						try { if (os != null) os.close(); } catch(IOException e) { }
						try { if (conexion != null) conexion.close(); } catch(IOException e) { }
					}
			try { puntuaciones.guarda(); } catch(Exception e) {}
			apv.puntuacionesEnviadas();
		}
	}
}