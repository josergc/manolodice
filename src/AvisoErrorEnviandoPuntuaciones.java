/**
	Nombre del proyecto:
	Comunes

	Descripci�n de la clase:
	Aviso de error en el env�o de las puntuaciones
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 26/11/2006
*/
import javax.microedition.lcdui.*;

public interface AvisoErrorEnviandoPuntuaciones {
	public void errorEnviandoPuntuaciones(String mensaje);
}