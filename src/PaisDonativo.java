/**
	Proyecto:
	Comunes
	
	Hist�rico de versiones:
	1.0 - 7 de noviembre de 2006
	
	Descripci�n de la clase:
	Lista de pa�ses desde donde se pueden hacer env�os de SMS
	
	Autor:
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk

	Licencia:
	GNU/GPL Versi�n 2.0
*/

public class PaisDonativo {
	public String pais;
	public String cadenaAEnviar;
	public String telefono;
	public PaisDonativo(String pais, String cadenaAEnviar, String telefono) {
		this.pais = pais;
		this.cadenaAEnviar = cadenaAEnviar;
		this.telefono = telefono;
	}
	public static final PaisDonativo[] paises = {
		new PaisDonativo("Alemania","OPEN SEPOMO","81444"),
		new PaisDonativo("Austria","OPEN SEPOMO","0900242202"),
		new PaisDonativo("B�lgica","EM SEPOMO","7222"),
		new PaisDonativo("Chile","EN SEPOMO","7887"),
		new PaisDonativo("Colombia","EN SEPOMO","7766"),
		new PaisDonativo("Espa�a","EN SEPOMO","5522"),
		new PaisDonativo("Holanda","EM SEPOMO","7222"),
		new PaisDonativo("Portugal","EN SEPOMO","4224"),
		new PaisDonativo("Reino Unido","EN SEPOMO","69569"),
		new PaisDonativo("Suiza","OPEN SEPOMO","83111"),
		new PaisDonativo("Venezuela","EN SEPOMO","7766")
	};
}