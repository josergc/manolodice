/**
	Nombre del proyecto:
	Comunes

	Descripci�n de la clase:
	Algunas constantes globales para los juegos.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.1 - 11/11/2006
		A�adida la constante
			TOPRIGHT
	1.0 - 05/11/2006
*/

import java.util.*;
import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;

public class Constantes extends Canvas {
	public static int centroX;
	public static int centroY;
	public static int anchoPantalla;
	public static int altoPantalla;
	public static int minTamPantalla;
	public static int maxTamPantalla;
	public static int botonArriba = UP;
	public static int botonAbajo = DOWN;
	public static int botonIzquierda = LEFT;
	public static int botonDerecha = RIGHT;
	public static int botonDisparo = FIRE;
	public static final int TOPLEFT = Graphics.TOP | Graphics.LEFT;
	public static final int TOPHCENTER = Graphics.TOP | Graphics.HCENTER;
	public static final int TOPRIGHT = Graphics.TOP | Graphics.RIGHT;
	public static Display d;
	public static Random r = new Random(new Date().getTime());
	public static Font f;
	public static MIDlet m;
	public static boolean orientacionVertical;

	public Constantes(MIDlet m) {
		this.m = m;
		centroX = (anchoPantalla = getWidth()) >> 1;
		centroY = (altoPantalla = getHeight()) >> 1;
		if (anchoPantalla < altoPantalla) {
			minTamPantalla = anchoPantalla;
			maxTamPantalla = altoPantalla;
			orientacionVertical = true;
		} else {
			minTamPantalla = altoPantalla;
			maxTamPantalla = anchoPantalla;
			orientacionVertical = false;
		}
		d = Display.getDisplay(m);
		f = Font.getDefaultFont();
	}
	public void paint(Graphics g){}
}
	
