/**
	Nombre del proyecto:
	Comunes

	Descripci�n de la clase:
	En esta pantalla podemos introducir el c�digo recibido para hacer el 
	micropago efectivo.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 05/11/2006
*/

import java.io.*;
import javax.microedition.io.*;
import javax.microedition.lcdui.*;

public class PantallaIntroducirCodigoMicropago extends Form {
	protected TextField codigo = new TextField(
		"Introduce aqu� el c�digo",
		"",
		50,
		TextField.ANY
		);

	public PantallaIntroducirCodigoMicropago() {
		super("Introducir c�digo");
		
		append(new StringItem(
			"Confirmar micropago",
			"En esta pantalla puedes hacer efectivo tu micropago introduciendo el c�digo del SMS recibido."
			));
		append(codigo);
	}
	
	public String devCodigo() {
		return codigo.getString();
	}
}
