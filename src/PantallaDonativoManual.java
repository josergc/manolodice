/**
	Nombre del proyecto:
	Comunes

	Descripci�n de la clase:
	En esta pantalla permitimos al usuario introducir el n�mero de tel�fono y 
	el mensaje a enviar
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 05/11/2006
*/

import javax.microedition.lcdui.*;

public class PantallaDonativoManual extends Form {
	protected TextField mensaje;
	protected TextField telefono;
	
	public PantallaDonativoManual() {
		super("Env�o del donativo manual");
		
		append(new StringItem("Atenci�n","Esta forma de env�o es para el caso en el que se a�adiera alg�n pa�s a la lista despu�s de la publicaci�n o que se hubiera cambiado el mensaje a enviar o el tel�fono a donde enviar el mensaje. M�s informaci�n en el apartado de \'donativos\' en www.josergc.tk"));
		append(mensaje = new TextField("Mensaje","EN SEPOMO",255,TextField.ANY));
		append(telefono = new TextField("Tel�fono","5522",255,TextField.PHONENUMBER));
	}
	
	public String devMensaje() {
		return mensaje.getString();
	}
	
	public String devTelefono() {
		return telefono.getString();
	}
}
