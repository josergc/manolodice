/**
	Nombre del proyecto:
	Comunes

	Descripci�n de la clase:
	Aviso de puntuaciones enviadas
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 26/11/2006
*/
import javax.microedition.lcdui.*;

public interface AvisoPuntuacionesEnviadas {
	public void puntuacionesEnviadas();
}