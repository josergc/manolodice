/**
	Nombre del proyecto:
	Comunes

	Descripci�n de la clase:
	Interfaz para poder definir un manejador de puntuaciones
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 14/11/2006
*/

public interface ManejadorPuntuaciones {
	public Object[][] devPuntuaciones();
	public void insertarPuntuacion(int puntos);
}