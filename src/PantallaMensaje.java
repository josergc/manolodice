/**
	Nombre del proyecto:
	Comunes

	Descripci�n de la clase:
	Muestra una pantalla con un mensaje dado.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 05/11/2006
*/

import java.io.*;
import javax.microedition.lcdui.*;

public class PantallaMensaje extends Form {
	protected static Image imgMensaje = null;
	protected Displayable pantallaRetorno;
	public PantallaMensaje(String mensaje, Displayable pantallaRetorno) {
		super("Mensaje");
		inicia("Mensaje",mensaje,pantallaRetorno);
	}
	public PantallaMensaje(String titulo, String mensaje, Displayable pantallaRetorno) {
		super("Mensaje");
		inicia(titulo,mensaje,pantallaRetorno);
	}
	protected void inicia(String titulo, String mensaje, Displayable pantallaRetorno) {
		this.pantallaRetorno = pantallaRetorno;
		if (imgMensaje == null)
			try {
				imgMensaje = Image.createImage("/mensaje.png");
			} catch (IOException e) {
			}
		append(new ImageItem("",imgMensaje,ImageItem.LAYOUT_CENTER,""));
		append(new StringItem(titulo,null));
		String[] cadenas = Utiles.divideCadena(mensaje,'\n');
		for (int i = 0; i < cadenas.length; i++)
			append(new StringItem(null,cadenas[i]));
	}
	public Displayable devPantallaRetorno() {
		return pantallaRetorno;
	}
}