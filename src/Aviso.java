import javax.microedition.lcdui.*;

public interface Aviso {
	public void avisar(Displayable d, Runnable r, String titulo, String mensaje, int tipo);
}