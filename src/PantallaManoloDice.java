/**
	Proyecto:
	Manolo dice...
	
	Descripci�n de la clase:
	Esta clase representa la pantalla del juego y sus diversos estados.
	
	Autor:
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk

	Licencia:
	GNU/GPL Versi�n 2.0
	
	Hist�rico de versiones:
	1.0 - 11 de noviembre de 2006
*/
import java.util.*;
import javax.microedition.lcdui.*;

public class PantallaManoloDice extends Canvas implements Runnable {
	/*
		Variables y estado del juego
	*/
	public static final int TOPLEFT = Graphics.TOP | Graphics.LEFT;
	public static final int TOPRIGHT = Graphics.TOP | Graphics.RIGHT;
	public static final int TOPHCENTER = Graphics.TOP | Graphics.HCENTER;
	
	public static final Font f = Font.getDefaultFont();
	public static final int altoF = f.getHeight();
	public static Font fNegrita = f;
	public static int altoFNegrita = fNegrita.getHeight();
	public static Font fCursiva = f;
	public static int altoFCursiva = fCursiva.getHeight();
	public static Font fNegritaCursiva = f;
	public static int altoFNegritaCursiva = fNegritaCursiva.getHeight();
	
	static {
		try { fNegrita = Font.getFont(f.getFace(),Font.STYLE_BOLD,f.getSize()); } catch(Exception e) { fNegrita = f;}
		altoFNegrita = fNegrita.getHeight();
		try { fCursiva = Font.getFont(f.getFace(),Font.STYLE_ITALIC,f.getSize()); } catch(Exception e) { fCursiva = f;}
		altoFCursiva = fCursiva.getHeight();
		try { fNegritaCursiva = Font.getFont(f.getFace(),Font.STYLE_BOLD | Font.STYLE_ITALIC,f.getSize()); } catch(Exception e) { fNegritaCursiva = f;}
		altoFNegritaCursiva = fNegritaCursiva.getHeight();
	}
	
	
	protected Random r = new Random(new Date().getTime());

	protected boolean ejecuta = true;
	protected boolean iniciaBuffers = true;
	protected boolean buffer0 = true;
	protected Image[] buffer = new Image[2];
	protected Graphics[] gBuffer = new Graphics[buffer.length];
	protected Graphics gActual;
	protected int anchoBuffer;
	protected int altoBuffer;
	protected int minBuffer;
	protected int centroX;
	protected int centroY;

	protected int diametro;
	protected int radio;

	public static final int BLANCO = 0xffffff;
	public static final int AMARILLO = 0xffff00;
	public static final int VERDE = 0x00ff00;
	public static final int VERDE_OSCURO = 0x007700;
	public static final int ROJO = 0xff0000;
	public static final int ROJO_OSCURO = 0x770000;
	public static final int NEGRO = 0x000000;
	public static final int GRIS_OSCURO = 0x333333;
	public static final int GRIS_CLARO = 0xaaaaaa;
	public static final int AZUL = 0x0000ff;
	public static final int NARANJA = 0xffaa00;
	public static final int LILA = 0xff00aa;
	public static final int CYAN = 0x00aaff;
	
	protected int[] colorIluminado = new int[] { 
		0x0000ff, 0xff0000, 0x00ff00, 0xffff00, 0xffffff, 0xff7700, 0x00ffff, 0xff00ff
		};
	protected int[] colorApagado;
	protected boolean[] estadoTablero;
	protected static final int SIN_INICIAR = 0;
	protected static final int EMPEZAR = 1;
	protected static final int PINTA_SECUENCIA = 2;
	protected static final int TURNO_JUGADOR = 3;
	protected static final int RESULTADO = 4;
	protected int estado = SIN_INICIAR;
	protected int[] secuenciaActual = null;
	protected int[] respuestaJugador = null;
	protected int jugadas = 0;
	protected int dificultad = 0;
	protected int tiempoEspera = 25;
	protected final static int nSectores = 4;
	
	/*
		Funciones generales en el juego
	*/

	protected void pintaTablero(Graphics g) {
		for (int i = 0, a = 45; i < nSectores; i++, a += 360 / nSectores) {
			g.setColor(
				estadoTablero[i] ? colorIluminado[i] : colorApagado[i]
				);
			g.fillArc(
				centroX - radio, 
				centroY - radio,
				diametro,
				diametro,
				a,
				360 / nSectores
				);
			g.setColor(
				colorIluminado[i]
				);
			g.drawArc(
				centroX - radio, 
				centroY - radio,
				diametro,
				diametro,
				a,
				360 / nSectores
				);
			String s = new Integer(i + 1).toString();
		}

	}

	protected int[] generaNuevaSecuencia() {
		int[] secuencia = new int[4 + dificultad];
		for (int i = 0; i < secuencia.length; i++) {
			secuencia[i] = Math.abs(r.nextInt()) % nSectores;
		}
		return secuencia;
	}

	public void empezar() {
		cambiaEstado(EMPEZAR);
	}
	
	protected void cambiaEstado(int nuevoEstado) {
		switch(estado) {
			case EMPEZAR:
				finEmpezar();
			break;
			case PINTA_SECUENCIA:
				finPintaSecuencia();
			break;
			case TURNO_JUGADOR:
				finTurnoJugador();
			break;
			case RESULTADO:
				finResultado();
			break;
		}
		switch(estado = nuevoEstado) {
			case EMPEZAR:
				iniciaEmpezar();
			break;
			case PINTA_SECUENCIA:
				iniciaPintaSecuencia();
			break;
			case TURNO_JUGADOR:
				iniciaTurnoJugador();
			break;
			case RESULTADO:
				iniciaResultado();
			break;
		}
		
	}

	/*
		Muestra un mensaje de que est� listo para empezar y lo alterna junto a 
		la tabla de puntuaciones
	*/
	protected int yPuntuaciones;
	protected Object[][] puntuaciones;
	protected int pintaMensaje = 0;
	protected static final int tiempoCambioDePantallaEmpezar = 60;
	protected int temporizadorCambioDePantallaEmpezar = tiempoCambioDePantallaEmpezar;
	protected boolean mostrarPuntuaciones = false;
	protected static final String mensajeEmpezar = "Pulsa una tecla para empezar...";
	protected String[] cadenasMensajeEmpezar;
	protected int anchoMensajeEmpezar = 0;
	protected static final Font fuenteMensajeEmpezar = fNegrita;
	protected static final int altoFuenteMensajeEmpezar = altoFNegrita;
	protected static final String mensajePuntuaciones = "Puntuaciones";
	protected static final Font fuenteMensajePuntuaciones = fNegrita;
	protected static final int anchoMensajePuntuaciones = fuenteMensajePuntuaciones.stringWidth(mensajePuntuaciones);
	protected static final int altoFuenteMensajePuntuaciones = altoFNegrita;
	protected static final Font fuenteMensajePuntos = f;
	protected static final int altoFuenteMensajePuntos = altoF;
	protected int anchoTablaPuntuaciones = 0;
	protected int altoTablaPuntuaciones = 0;
	protected int ruleta = 0;
	protected static final int finTemporizadorRuleta = 6;
	protected int temporizadorRuleta = finTemporizadorRuleta + 1;
	protected void iniciaEmpezar() {
		pintaMensaje = 0;
		jugadas = 0;
		dificultad = 0;
		tiempoEspera = 5;
		temporizadorCambioDePantallaEmpezar = tiempoCambioDePantallaEmpezar;
		mostrarPuntuaciones = false;
		puntuaciones = manejadorPuntuaciones.devPuntuaciones();
		
		// Calcula el ancho de la tabla de puntuaciones
		anchoTablaPuntuaciones = anchoMensajePuntuaciones;
		for (int i = 0, ancho; i < puntuaciones.length; i++) {
			ancho = (fuenteMensajePuntos.stringWidth((String)puntuaciones[i][0]) +
				4 + 
				fuenteMensajePuntos.stringWidth(((Integer)puntuaciones[i][1]).toString())
				) << 1;
			if (ancho > anchoTablaPuntuaciones)
				anchoTablaPuntuaciones = ancho;
		}
		// Calcula el alto de la tabla de puntuaciones
		altoTablaPuntuaciones = puntuaciones.length * fuenteMensajePuntuaciones.getHeight();
		// Limpia el tablero
		for (int i = 0; i < estadoTablero.length; i++)
			estadoTablero[i] = false;
		ruleta = 0;
	}
	protected void finEmpezar() {
	}
	protected void ejecutaEmpezar() {
		if (!mostrarPuntuaciones) 
			pintaMensaje = (pintaMensaje + 1) & 0x7;
		temporizadorCambioDePantallaEmpezar--;
		if (temporizadorCambioDePantallaEmpezar == 0) {
			temporizadorCambioDePantallaEmpezar = tiempoCambioDePantallaEmpezar;
			mostrarPuntuaciones = !mostrarPuntuaciones;
		}
		
		if (temporizadorRuleta > finTemporizadorRuleta) {
			estadoTablero[ruleta] = false;
			ruleta = (ruleta + 1) % estadoTablero.length;
			estadoTablero[ruleta] = true;
			temporizadorRuleta = 0;
		}
		temporizadorRuleta++;
	}
	protected void pintaEmpezar(Graphics g) {
		pintaTablero(g);
		if (mostrarPuntuaciones) {
			g.setFont(fuenteMensajePuntuaciones);
			yPuntuaciones = (altoBuffer - altoFuenteMensajePuntuaciones * (puntuaciones.length + 1)) >> 1;
			g.setColor(AZUL);
			g.fillRect(
				centroX - (anchoTablaPuntuaciones >> 1) - 2,
				yPuntuaciones,
				anchoTablaPuntuaciones,
				altoFuenteMensajePuntuaciones
				);
			g.setColor(NEGRO);
			g.drawString(mensajePuntuaciones,centroX,yPuntuaciones,TOPHCENTER);
			yPuntuaciones += altoFuenteMensajePuntuaciones;
			g.setColor(BLANCO);
			g.fillRect(
				centroX - (anchoTablaPuntuaciones >> 1) - 2,
				yPuntuaciones,
				anchoTablaPuntuaciones,
				altoTablaPuntuaciones
				);
			g.setColor(NEGRO);
			g.setFont(fuenteMensajePuntos);
			for (int i = 0, y = yPuntuaciones; i < puntuaciones.length; i++, y += altoFuenteMensajePuntuaciones) {
				g.drawString((String)puntuaciones[i][0],centroX - 2,y,TOPRIGHT);
				g.drawString(((Integer)puntuaciones[i][1]).toString(),centroX + 2,y,TOPLEFT);
			}
		} else {
			if (pintaMensaje != 0) {
				g.setColor(BLANCO);
				g.fillRect(
					0,
					centroY - ((altoFNegrita * cadenasMensajeEmpezar.length) >> 1),
					anchoBuffer,
					altoFNegrita * cadenasMensajeEmpezar.length
					);
				g.setColor(NEGRO);
				g.setFont(fNegrita);
				Utiles.pintaCadenasCentradas(
					g,
					cadenasMensajeEmpezar,
					centroX,
					centroY - ((altoFNegrita * cadenasMensajeEmpezar.length) >> 1)
					);
			}
		}
	}
	protected void teclaEmpezar(int keyCode) {
		if (mostrarPuntuaciones) {
			temporizadorCambioDePantallaEmpezar = tiempoCambioDePantallaEmpezar;
			mostrarPuntuaciones = false;
		} else
			cambiaEstado(PINTA_SECUENCIA);
	}

	/*
		Pinta la secuencia
	*/
	protected boolean pinta;
	protected int enumSecuencia;
	protected int tiempoEsperaActual = tiempoEspera;
	protected void iniciaPintaSecuencia() {
		secuenciaActual = generaNuevaSecuencia();
		respuestaJugador = new int[secuenciaActual.length];
		enumSecuencia = 0;
		pinta = true;
		tiempoEsperaActual = tiempoEspera;
	}
	protected void finPintaSecuencia() {
	}
	protected void ejecutaPintaSecuencia() {	
	}
	protected void pintaPintaSecuencia(Graphics g) {
		if (pinta) {
			if (enumSecuencia < secuenciaActual.length) {
				for (int i = 0; i < estadoTablero.length; i++) {
					estadoTablero[i] = secuenciaActual[enumSecuencia] == i;
				}
			} else {
				for (int i = 0; i < estadoTablero.length; i++) {
					estadoTablero[i] = false;
				}
				cambiaEstado(TURNO_JUGADOR);
			}
		} else {
			for (int i = 0; i < estadoTablero.length; i++) {
				estadoTablero[i] = false;
			}
		}
		if (tiempoEsperaActual == 0) {
			pinta = !pinta;
			if (pinta) {
				tiempoEsperaActual = tiempoEspera;
				enumSecuencia++;
			} else
				tiempoEsperaActual = tiempoEspera >> 1;
		} else {
			tiempoEsperaActual--;
		}
		pintaTablero(g);
	}
	protected void teclaPintaSecuencia(int keyCode) {
	}
	
	/*
		Pinta las pulsaciones del jugador
	*/
	protected int iRespuestaJugador;
	protected void iniciaTurnoJugador() {
		iRespuestaJugador = 0;
	}
	protected void finTurnoJugador() {
	}
	protected void ejecutaTurnoJugador() {
	}
	protected void pintaTurnoJugador(Graphics g) {
		pintaTablero(g);
		synchronized(estadoTablero) {
			for (int i = 0; i < estadoTablero.length; i++)
				estadoTablero[i] = false;
		}
	}
	protected void teclaTurnoJugador(int keyCode) {
		int t = keyCode;
		int a = getGameAction(keyCode);
		if (a == UP || a == LEFT || a == RIGHT || a == DOWN) {
			synchronized(estadoTablero) {
				estadoTablero[0] = a == UP;
				estadoTablero[1] = a == LEFT;
				estadoTablero[2] = a == DOWN;
				estadoTablero[3] = a == RIGHT;
				if (a == UP)
					respuestaJugador[iRespuestaJugador++] = 0;
				else if (a == LEFT)
					respuestaJugador[iRespuestaJugador++] = 1;
				else if (a == DOWN)
					respuestaJugador[iRespuestaJugador++] = 2;
				else if (a == RIGHT)
					respuestaJugador[iRespuestaJugador++] = 3;
				if (iRespuestaJugador >= respuestaJugador.length)
					cambiaEstado(RESULTADO);
			}
		}
	}

	/*
		Muestra el resultado
	*/
	protected boolean resultadoJugada;
	protected static final String mensajeResultado = "Resultado de la jugada ";
	protected String[] cadenasMensajeResultado = null;
	protected int anchoMensajeResultado;
	protected static final Font fuenteMensajeResultado = fNegrita;
	protected static final int altoFuenteMensajeResultado = altoFNegrita;
	protected static final String mensajeHasPerdido = "Has perdido... pulsa una tecla.";
	protected String[] cadenasMensajeHasPerdido;
	protected int anchoMensajeHasPerdido;
	protected static final Font fuenteMensajeHasPerdido = fCursiva;
	protected static final int altoFuenteMensajeHasPerdido = altoFCursiva;
	protected static final String mensajeHasGanado = "Has ganado, pulsa una tecla para continuar.";
	protected String[] cadenasMensajeHasGanado;
	protected int anchoMensajeHasGanado;
	protected static final Font fuenteMensajeHasGanado = fCursiva;
	protected static final int altoFuenteMensajeHasGanado = altoFCursiva;
	protected int xPresentacionSecuencia;
	protected int yPresentacionSecuencia;
	protected void iniciaResultado() {
		int i = 0;
		boolean continuarComprobando = true;
		while (continuarComprobando)
			if (i < respuestaJugador.length) {
				if (continuarComprobando = (secuenciaActual[i] == respuestaJugador[i]))
					i++;
			} else
				continuarComprobando = false;
		resultadoJugada = i == respuestaJugador.length;
		xPresentacionSecuencia = centroX;
		f.stringWidth(mensajeResultado + (jugadas + 1));
		cadenasMensajeResultado = Utiles.divideCadena(
			mensajeResultado + (jugadas + 1),
			getWidth(),
			fuenteMensajeResultado
			);
		anchoMensajeResultado = anchoMaximoCadenas(
			cadenasMensajeResultado,
			fuenteMensajeResultado
			);
	}
	protected void finResultado() {
		cadenasMensajeResultado = null;
	}
	protected void ejecutaResultado() {
		xPresentacionSecuencia -= 2;
		if (xPresentacionSecuencia + altoF * (secuenciaActual.length << 1) <= 0)
			xPresentacionSecuencia = anchoBuffer;
	}
	protected void pintaResultado(Graphics g) {
		// Pinta el mensaje con el resultado de la jugada
		g.setColor(NARANJA);
		g.fillRect(
			0,
			0,
			anchoBuffer,
			cadenasMensajeResultado.length * altoFuenteMensajeHasGanado
			);
		g.setFont(fuenteMensajeResultado);
		g.setColor(NEGRO);
		Utiles.pintaCadenasCentradas(
			g,
			cadenasMensajeResultado,
			centroX,
			0
			);
		yPresentacionSecuencia = altoFuenteMensajeResultado * cadenasMensajeResultado.length + ((
			altoBuffer - 
			(resultadoJugada ? altoFuenteMensajeHasGanado * cadenasMensajeHasGanado.length : altoFuenteMensajeHasPerdido * cadenasMensajeHasPerdido.length) - 
			altoFuenteMensajeResultado * cadenasMensajeResultado.length
			) >> 1);
		
		// Pinta la secuencia correcta y la del jugador
		for (int i = 0; i < secuenciaActual.length; i++) {
			g.setColor(colorIluminado[secuenciaActual[i]]);
			g.fillArc(
				xPresentacionSecuencia + ((i * altoF) << 1),
				yPresentacionSecuencia - altoF - 1,
				altoF,
				altoF,
				0,
				360
				);
			g.setColor(colorIluminado[respuestaJugador[i]]);
			g.fillArc(
				xPresentacionSecuencia + ((i * altoF) << 1),
				yPresentacionSecuencia + 1,
				altoF,
				altoF,
				0,
				360
				);
		}
		
		// Pinta le mensaje de si el jugador ha perdido o ha ganado
		if (resultadoJugada) {
			g.setFont(fuenteMensajeHasGanado);
			g.setColor(CYAN);
			g.fillRect(
				0,
				altoBuffer - altoFuenteMensajeHasGanado * cadenasMensajeHasGanado.length,
				anchoBuffer,
				altoFuenteMensajeHasGanado * cadenasMensajeHasGanado.length
				);
			g.setColor(NEGRO);
			Utiles.pintaCadenasCentradas(
				g,
				cadenasMensajeHasGanado,
				centroX,
				altoBuffer - altoFuenteMensajeHasGanado * cadenasMensajeHasGanado.length
				);
		} else {
			g.setFont(fuenteMensajeHasPerdido);
			g.setColor(LILA);
			g.fillRect(
				0,
				altoBuffer - altoFuenteMensajeHasPerdido * cadenasMensajeHasPerdido.length,
				anchoBuffer,
				altoFuenteMensajeHasPerdido * cadenasMensajeHasPerdido.length
				);
			g.setColor(NEGRO);
			Utiles.pintaCadenasCentradas(
				g,
				cadenasMensajeHasPerdido,
				centroX,
				altoBuffer - altoFuenteMensajeHasPerdido * cadenasMensajeHasPerdido.length
				);
		}
	}
	protected void teclaResultado(int keyCode) {
		if (resultadoJugada) {
			if (tiempoEspera > 2) {
				tiempoEspera--;
			} else {
				dificultad++;
				tiempoEspera = 5;
			}
			jugadas++;
			cambiaEstado(PINTA_SECUENCIA);
		} else {
			manejadorPuntuaciones.insertarPuntuacion(jugadas);
			cambiaEstado(EMPEZAR);
		}
	}

	/*
		Funciones de iniciaci�n y gesti�n generales del juego
	*/
	
	protected AvisoRepintar avisoRepintar;
	protected Aviso aviso;
	protected ManejadorPuntuaciones manejadorPuntuaciones;

	public PantallaManoloDice(AvisoRepintar avisoRepintar, Aviso aviso, ManejadorPuntuaciones manejadorPuntuaciones) {
		this.avisoRepintar = avisoRepintar;
		this.aviso = aviso;
		this.manejadorPuntuaciones = manejadorPuntuaciones;
		
		// Inicia los colores
		colorApagado = new int[nSectores];
		estadoTablero = new boolean[nSectores];
		for (int i = 0; i < nSectores; i++) {
			colorApagado[i] = colorIluminado[i] & 0x303030;
			estadoTablero[i] = false;
		}
		
		cambiaEstado(EMPEZAR);
	}
	
	protected int anchoMaximoCadenas(String[] cadenas, Font f) {
		int max = 0, ancho;
		for (int i = 0; i < cadenas.length; i++) {
			ancho = f.stringWidth(cadenas[i]);
			if (ancho > max) max = ancho;
		}
		return max;
	}
	
	public void paint(Graphics g) {
		if (iniciaBuffers) {
			centroX = (anchoBuffer = getWidth()) >> 1;
			centroY = (altoBuffer = getHeight()) >> 1;

			radio = (diametro = (minBuffer = (anchoBuffer < altoBuffer ? anchoBuffer : altoBuffer)) * 9 / 10) >> 1;

			// Parte las cadenas seg�n el ancho de la pantalla
			cadenasMensajeEmpezar = Utiles.divideCadena(
				mensajeEmpezar,
				getWidth(),
				fuenteMensajeEmpezar
				);
			anchoMensajeEmpezar = anchoMaximoCadenas(
				cadenasMensajeEmpezar,
				fuenteMensajeEmpezar
				);

			cadenasMensajeHasPerdido = Utiles.divideCadena(
				mensajeHasPerdido,
				getWidth(),
				fuenteMensajeHasPerdido
				);
			anchoMensajeHasPerdido = anchoMaximoCadenas(
				cadenasMensajeHasPerdido,
				fuenteMensajeHasPerdido
				);

			cadenasMensajeHasGanado = Utiles.divideCadena(
				mensajeHasGanado,
				getWidth(),
				fuenteMensajeHasGanado
				);
			anchoMensajeHasGanado = anchoMaximoCadenas(
				cadenasMensajeHasGanado,
				fuenteMensajeHasGanado
				);

			// Crea doble buffer
			buffer[0] = Image.createImage(anchoBuffer,altoBuffer);
			buffer[1] = Image.createImage(anchoBuffer,altoBuffer);
			gBuffer[0] = buffer[0].getGraphics();
			gActual = gBuffer[1] = buffer[1].getGraphics();
			
			iniciaBuffers = false;
		}
		
		// Pinta la pantalla
		gActual.setColor(GRIS_OSCURO);
		gActual.fillRect(0,0,anchoBuffer,altoBuffer);

		switch(estado) {
			case EMPEZAR:
				pintaEmpezar(gActual);
			break;
			case PINTA_SECUENCIA:
				pintaPintaSecuencia(gActual);
			break;
			case TURNO_JUGADOR:
				pintaTurnoJugador(gActual);
			break;
			case RESULTADO:
				pintaResultado(gActual);
			break;
		}

		g.drawImage(buffer[buffer0 ? 0 : 1],0,0,TOPLEFT);
		
		// Alterna el buffer actual
		buffer0 = !buffer0;
		gActual = gBuffer[buffer0 ? 1 : 0];
	}

	public void run() {
		switch(estado) {
			case EMPEZAR:
				ejecutaEmpezar();
			break;
			case PINTA_SECUENCIA:
				ejecutaPintaSecuencia();
			break;
			case TURNO_JUGADOR:
				ejecutaTurnoJugador();
			break;
			case RESULTADO:
				ejecutaResultado();
			break;
		}
		repaint();
		try { Thread.sleep(40); } catch(InterruptedException e) {}
		avisoRepintar.repintar(this);
	}

	protected void keyPressed(int keyCode) {
		switch(estado) {
			case EMPEZAR:
				teclaEmpezar(keyCode);
			break;
			case PINTA_SECUENCIA:
				teclaPintaSecuencia(keyCode);
			break;
			case TURNO_JUGADOR:
				teclaTurnoJugador(keyCode);
			break;
			case RESULTADO:
				teclaResultado(keyCode);
			break;
		}
	}
}
