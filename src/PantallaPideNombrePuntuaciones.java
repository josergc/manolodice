/**
	Nombre del proyecto:
	Comunes

	Descripci�n de la clase:
	Muestra una pantalla que pide el nombre para las puntuaciones.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 26/11/2006
*/

import java.io.*;
import javax.microedition.lcdui.*;

public class PantallaPideNombrePuntuaciones extends Form {
	protected TextField nombre;
	public PantallaPideNombrePuntuaciones() {
		super("Ponga su nombre");
		append(nombre = new TextField("","",10,TextField.ANY));
	}
	public void ponPuntos(int puntos) {
		nombre.setLabel(puntos + " puntos");
	}
	public String devNombre() {
		return nombre.getString();
	}
}