/**
	Nombre del proyecto:
	Comunes

	Descripci�n de la clase:
	En esta pantalla se env�a autom�ticamente un mensaje SMS para solicitar un 
	c�digo para realizar un micropago.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.1 - 15/12/2006
		Al reenviar, hab�a un fallo por haberse dejado la conexi�n anterior 
		abierta. He puesto un "close" expl�cito en el finally
	1.0 - 05/11/2006
*/

import java.io.*;
import javax.microedition.io.*;
import javax.microedition.lcdui.*;
import javax.wireless.messaging.*;

public class PantallaEnviarMensaje extends Form implements Runnable {
	protected String mensajeAEnviar;
	protected String telefono;
	protected PantallaHacerDonativo phd;
	
	public PantallaEnviarMensaje(PantallaHacerDonativo phd, String mensajeAEnviar, String telefono) {
		super("Enviando mensaje...");
		
		this.phd = phd;
		this.mensajeAEnviar = mensajeAEnviar;
		this.telefono = telefono;
	}
	
	public void run() {
		// Env�a el mensaje
		synchronized (this) {
			MessageConnection mc = null;
			try {
				StringItem si;
				append(si = new StringItem("Estableciendo conexi�n...",null));
				mc = (MessageConnection)Connector.open("sms://" + telefono);
				si.setText("�hecho!");
				append(si = new StringItem("Creando mensaje...",null));
				TextMessage mensaje = (TextMessage)mc.newMessage(MessageConnection.TEXT_MESSAGE);
				si.setText("�hecho!");
				append(si = new StringItem("Poniendo mensaje en la bandeja de salida...",null));
				mensaje.setPayloadText(mensajeAEnviar);
				si.setText("�hecho!");
				append(si = new StringItem("Enviando mensaje...",null));
				mc.send(mensaje);
				si.setText("�hecho!");
				phd.muestraMensaje(
					"Mensaje enviado",
					"Ahora deber�a de recibir un SMS con el c�digo e ir al apartado 'donativos' en www.josergc.tk para hacer efectiva tu donaci�n."
					);
			} catch (IllegalArgumentException e) {
				phd.muestraError(
					"No se ha podido enviar el mensaje (IllegalArgumentException)",
					e.toString()
					);
			} catch (IOException e) {
				phd.muestraError(
					"No se ha podido enviar el mensaje (IOException)",
					e.toString()
					);
			} catch (Exception e) {
				phd.muestraError(
					"No se ha podido enviar el mensaje (Exception)",
					e.toString()
					);
			} finally {
				if (mc != null) try {
					mc.close();
				} catch(IOException e) {
					// Error al cerrar el fichero
				}
			}
		}
	}
}