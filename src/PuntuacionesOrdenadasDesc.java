/**
	Nombre del proyecto:
	Comunes

	Descripci�n de la clase:
	Guarda las puntuaciones del juego que se le diga
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 13/11/2006
*/
import java.io.*;
import java.util.*;
import javax.microedition.rms.*;

public class PuntuacionesOrdenadasDesc implements RecordComparator {
	protected DataInputStream dis1, dis2;
	protected int puntos1, puntos2;
	public int compare(byte[] rec1, byte[] rec2) {
		try {
			dis1 = new DataInputStream(new ByteArrayInputStream(rec1));
			dis2 = new DataInputStream(new ByteArrayInputStream(rec2));
			if ((puntos1 = dis1.readInt()) < (puntos2 = dis2.readInt())) {
				return PRECEDES;
			} else if (puntos1 == puntos2) {
				// Si los puntos son iguales, el que sea m�s actual, ser� puesto 
				// antes
				return (dis1.readLong() < dis2.readLong()) ? FOLLOWS : PRECEDES;
			} 
			return FOLLOWS;
		} catch (IOException e) {
			return FOLLOWS;
		}
	}
}