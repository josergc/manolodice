/**
	Nombre del proyecto:
	Comunes

	Descripci�n de la clase:
	Esta pantalla muestra una lista con los pa�ses desde donde se pueden hacer
	micropagos con Sepomo.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 05/11/2006
*/

import javax.microedition.lcdui.*;

public class PantallaHacerMicropago extends List {
	public PantallaHacerMicropago() {
		super("Selecciona pa�s",IMPLICIT);
		
		for (int i = 0; i < PaisDonativo.paises.length; i++)
			append(PaisDonativo.paises[i].pais,null);
		append("--== Env�o manual ==--",null);
		append("--== Otros ==--",null);
	}
}