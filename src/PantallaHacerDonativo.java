/**
	Nombre del proyecto:
	Comunes

	Descripci�n de la clase:
	Esta pantalla muestra un texto explicando las distintas formas de hacer un 
	donativo.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 05/11/2006
*/

import javax.microedition.lcdui.*;

public class PantallaHacerDonativo extends Form implements CommandListener {
	protected static final Command cmdIntroducirCodigo = new Command("Introducir c�digo",Command.ITEM,1);
	protected static final Command cmdHacerMicropago = new Command("Hacer micropago",Command.ITEM,1);
	protected static final Command cmdVolver = new Command("Volver",Command.CANCEL,1);
	protected static final Command cmdAceptar = new Command("Aceptar",Command.OK,1);
	protected static final Command cmdCancelar = new Command("Cancelar",Command.CANCEL,1);
	
	protected AvisoRetornoDesdeDonativos pantallaRetorno;
	protected Display display;
	
	protected PantallaHacerMicropago pantallaHacerMicropago = null;
	protected PantallaIntroducirCodigoMicropago pantallaIntroducirCodigoMicropago = null;
	protected PantallaEnviarMensaje pantallaEnviarMensaje = null;
	protected PantallaDonativoManual pantallaDonativoManual = null;
	protected PantallaEnviarCodigoMicropago pantallaEnviarCodigoMicropago = null;
	protected PantallaError pantallaError = null;
	protected PantallaMensaje pantallaMensaje = null;

	public PantallaHacerDonativo(Display display, AvisoRetornoDesdeDonativos pantallaRetorno) {
		super("Haz un donativo");
		
		this.display = display;
		this.pantallaRetorno = pantallaRetorno;
		
		append(new StringItem(
			"Haz un donativo",
			"Si te ha gustado esta aplicaci�n, puedes hacer un donativo para animarme a realizar m�s proyectos."
			));
		append(new StringItem(
			"�C�mo hacerlo?","Puedes realizar tu donativo de diversas maneras."
			));
		append(new StringItem(
			"Mediante un micropago",
			"La aplicaci�n enviar� un SMS seleccionando antes el pa�s donde est�s. Deber�as recibir un SMS con un c�digo que o bien puedes introducirlo en el apartado 'Introducir c�digo' en esta pantalla o en el apartado 'Donativos' en www.josergc.tk"
			));
		append(new StringItem(
			"A trav�s de PayPal",
			"Si dispones de una cuenta de PayPal, puedes hacer tu donativo a josergc@lycos.es"
			));
		append(new StringItem(
			"Otros",
			"Si quieres hacer cualquier otro tipo de donativo o mediante otra manera, ponte en contacto conmigo enviando un e-mail a josergc@lycos.es"
			));
		append(new StringItem(
			"Ante todo",
			"Darte las gracias por usar la aplicaci�n y molestarte en leer esto :-)"
			));
		append(new StringItem(
			"El autor",
			"Jos� Roberto Garc�a Chico"
			));
			
		addCommand(cmdHacerMicropago);
		addCommand(cmdIntroducirCodigo);
		addCommand(cmdVolver);
		setCommandListener(this);
		
		display.setCurrent(this);
	}
	public void commandAction(Command c, Displayable d) {
		if (d == this) {
			if (c == cmdHacerMicropago) {
				if (pantallaHacerMicropago == null) {
					pantallaHacerMicropago = new PantallaHacerMicropago();
					pantallaHacerMicropago.addCommand(cmdHacerMicropago);
					pantallaHacerMicropago.addCommand(cmdVolver);
					pantallaHacerMicropago.setCommandListener(this);
				}
				display.setCurrent(pantallaHacerMicropago);
			} else if (c == cmdIntroducirCodigo) {
				if (pantallaIntroducirCodigoMicropago == null) {
					pantallaIntroducirCodigoMicropago = new PantallaIntroducirCodigoMicropago();
					pantallaIntroducirCodigoMicropago.addCommand(cmdAceptar);
					pantallaIntroducirCodigoMicropago.addCommand(cmdCancelar);
					pantallaIntroducirCodigoMicropago.setCommandListener(this);
				}
				display.setCurrent(pantallaIntroducirCodigoMicropago);
			} else if (c == cmdVolver) {			
				pantallaRetorno.retornoDesdeDonativos();
			}
		} else if (d == pantallaHacerMicropago) {
			if (c == cmdHacerMicropago || c == List.SELECT_COMMAND) {
				if (pantallaHacerMicropago.getSelectedIndex() != -1) {
					int indice = pantallaHacerMicropago.getSelectedIndex();
					if (indice == PaisDonativo.paises.length) {
						// Env�o manual
						pantallaDonativoManual = new PantallaDonativoManual();
						pantallaDonativoManual.addCommand(cmdAceptar);
						pantallaDonativoManual.addCommand(cmdCancelar);
						pantallaDonativoManual.setCommandListener(this);
						display.setCurrent(pantallaDonativoManual);
					} else if (indice == PaisDonativo.paises.length + 1) {
						// Otros
						muestraMensaje(
							null,
							"Si quieres enviar un donativo y tu pa�s no sale en la lista, es posible que quieras ver si se ha incorporado despu�s de la publicaci�n de esta aplicaci�n entrando en www.josergc.tk y en el apartado de \'Donativos\' o en www.sepomo.com"
							);
					} else {
						// Env�a el mensaje al pa�s elegido
						pantallaEnviarMensaje = new PantallaEnviarMensaje(
							this,
							PaisDonativo.paises[indice].cadenaAEnviar,
							PaisDonativo.paises[indice].telefono
							);
						display.setCurrent(pantallaEnviarMensaje);
						new Thread(pantallaEnviarMensaje).start();
					}
				}
			} else if (c == cmdVolver) {
				display.setCurrent(this);
			}
		} else if (d == pantallaIntroducirCodigoMicropago) {
			if (c == cmdAceptar) {
				pantallaEnviarCodigoMicropago = new PantallaEnviarCodigoMicropago(
					this,
					pantallaIntroducirCodigoMicropago.devCodigo()
					);
				display.setCurrent(pantallaEnviarCodigoMicropago);
				new Thread(pantallaEnviarCodigoMicropago).start();
			} else {
				display.setCurrent(this);
			}
		} else if (d == pantallaDonativoManual) {
			if (c == cmdAceptar) {
				pantallaEnviarMensaje = new PantallaEnviarMensaje(
					this,
					pantallaDonativoManual.devMensaje(),
					pantallaDonativoManual.devTelefono()
					);
				display.setCurrent(pantallaEnviarMensaje);
				new Thread(pantallaEnviarMensaje).start();
			} else {
				display.setCurrent(this);
			}
		} else if (d == pantallaMensaje) {
			display.setCurrent(pantallaMensaje.devPantallaRetorno());
		} else if (d == pantallaError) {
			display.setCurrent(pantallaError.devPantallaRetorno());
		}
	}
	
	public void muestraError(String titulo, String mensaje) {
		pantallaError = new PantallaError(titulo,mensaje,this);
		pantallaError.addCommand(cmdVolver);
		pantallaError.setCommandListener(this);
		display.setCurrent(pantallaError);
	}
	
	public void muestraMensaje(String titulo, String mensaje) {
		pantallaMensaje = new PantallaMensaje(titulo,mensaje,this);
		pantallaMensaje.addCommand(cmdVolver);
		pantallaMensaje.setCommandListener(this);
		display.setCurrent(pantallaMensaje);
	}
}