/**
	Nombre del proyecto:
	Comunes

	Descripci�n de la clase:
	Muestra una pantalla con un mensaje de error dado.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 05/11/2006
*/

import java.io.*;
import javax.microedition.lcdui.*;

public class PantallaError extends Form {
	protected static Image imgError = null;
	protected Displayable pantallaRetorno;
	public PantallaError(String mensaje, Displayable pantallaRetorno) {
		super("Error en la aplicaci�n");
		inicia("Mensaje de error",mensaje,pantallaRetorno);
	}
	public PantallaError(String titulo, String mensaje, Displayable pantallaRetorno) {
		super("Error en la aplicaci�n");
		inicia(titulo,mensaje,pantallaRetorno);
	}
	protected void inicia(String titulo, String mensaje, Displayable pantallaRetorno) {
		this.pantallaRetorno = pantallaRetorno;
		if (imgError == null)
			try {
				imgError = Image.createImage("/error.png");
			} catch (IOException e) {
			}
		append(new ImageItem("",imgError,ImageItem.LAYOUT_CENTER,""));
		append(new StringItem(titulo,null));
		String[] cadenas = Utiles.divideCadena(mensaje,'\n');
		for (int i = 0; i < cadenas.length; i++)
			append(new StringItem(null,cadenas[i]));
	}
	public Displayable devPantallaRetorno() {
		return pantallaRetorno;
	}
}