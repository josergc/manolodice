/**
	Nombre del proyecto:
	Comunes

	Descripci�n de la clase:
	Guarda las puntuaciones del juego que se le diga
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 13/11/2006
*/
import java.io.*;
import java.util.*;
import javax.microedition.rms.*;

public class Puntuaciones {
	protected RecordStore rsPuntuaciones;
	protected RecordComparator comparador;
	protected int[] recordId;
	protected String[] nombres;
	protected int[] puntos;
	protected long[] fechas;
	protected int[] niveles;
	protected boolean[] enviadas;

	public Puntuaciones(String nombreTabla, int nEntradas, RecordComparator comparador) throws 
		RecordStoreNotOpenException,
		RecordStoreException,
		RecordStoreNotFoundException,
		RecordStoreFullException,
		IllegalArgumentException,
		IOException
		{
		rsPuntuaciones = RecordStore.openRecordStore(
			nombreTabla + ".puntuaciones",
			true
			);
		recordId = new int[nEntradas];
		nombres = new String[nEntradas];
		puntos = new int[nEntradas];
		fechas = new long[nEntradas];
		niveles = new int[nEntradas];
		enviadas = new boolean[nEntradas];
		if (rsPuntuaciones.getNumRecords() != nEntradas) {
			long fechaActual = new Date().getTime();
			for (int i = 0; i < nEntradas; i++) {
				nombres[i] = "josergc";
				puntos[i] = 0;
				fechas[i] = fechaActual;
				niveles[i] = 0;
				enviadas[i] = true;
			}
		} else
			carga();
	}
	
	public void guarda() throws 
		RecordStoreNotOpenException,
		InvalidRecordIDException,
		RecordStoreException,
		IOException
		{
		byte[] ba;
		if (rsPuntuaciones.getNumRecords() != nombres.length) 
			for (int i = 0; i < nombres.length; i++) {
				ba = guardaPuntuacion(i);
				recordId[i] = rsPuntuaciones.addRecord(ba,0,ba.length);
			}
		else
			for (int i = 0; i < nombres.length; i++) {
				ba = guardaPuntuacion(i);
				rsPuntuaciones.setRecord(recordId[i],ba,0,ba.length);
			}
	}
	
	public void carga() throws
		RecordStoreNotOpenException,
		InvalidRecordIDException,
		RecordStoreException,
		IOException
		{
		RecordEnumeration re = rsPuntuaciones.enumerateRecords(null,comparador,false);
		for(int i = 0; re.hasNextElement(); i++) 
			cargaPuntuacion(
				i,
				rsPuntuaciones.getRecord(
					recordId[i] = re.nextRecordId()
					)
				);
	}
	
	protected byte[] guardaPuntuacion(int i) throws 
		IOException
		{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);
		dos.writeInt(puntos[i]);
		dos.writeLong(fechas[i]);
		dos.writeInt(niveles[i]);
		dos.writeBoolean(enviadas[i]);
		dos.writeUTF(nombres[i]);
		dos.flush();
		baos.flush();
		return baos.toByteArray();
	}
	
	protected void cargaPuntuacion(int i, byte[] ba) throws
		IOException
		{
		ByteArrayInputStream bais = new ByteArrayInputStream(ba);
		DataInputStream dis = new DataInputStream(bais);
		puntos[i] = dis.readInt();
		fechas[i] = dis.readLong();
		niveles[i] = dis.readInt();
		enviadas[i] = dis.readBoolean();
		nombres[i] = dis.readUTF();
	}
	
	public String devNombre(int i) {
		return new String(nombres[i]);
	}
	
	public int devPuntos(int i) {
		return puntos[i];
	}
	
	public int devNivel(int i) {
		return niveles[i];
	}
	
	public int devNumRegistros() {
		return nombres.length;
	}
	
	public boolean estaEnviada(int i) {
		return enviadas[i];
	}
	
	public void ponEnviado(int i) {
		enviadas[i] = true;
	}
	
	public boolean esInsertable(int nuevaPuntuacion) {
		for (int i = 0; i < puntos.length; i++)
			if (nuevaPuntuacion >= puntos[i])
				return true;
		return false;
	}
	
	public void insertaNuevaPuntuacion(String nombre, int nuevaPuntuacion, int nivel) throws 
		RecordStoreNotOpenException,
		InvalidRecordIDException,
		RecordStoreException,
		IOException
		{
		for (int i = 0; i < puntos.length; i++)
			if (nuevaPuntuacion >= puntos[i]) {
				for (int j = puntos.length - 1; j > i; j--) {
					nombres[j] = nombres[j - 1];
					puntos[j] = puntos[j - 1];
					fechas[j] = fechas[j - 1];
					niveles[j] = niveles[j - 1];
					enviadas[j] = enviadas[j - 1];
				}
				nombres[i] = nombre;
				puntos[i] = nuevaPuntuacion;
				fechas[i] = new Date().getTime();
				niveles[i] = nivel;
				enviadas[i] = false;
				guarda();
				return;
			}
	}
}