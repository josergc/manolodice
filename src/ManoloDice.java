/**
	Proyecto:
	Manolo dice...
	
	Hist�rico de versiones:
	1.0 - 11 de noviembre de 2006
	
	Descripci�n de la clase:
	Esta clase es la principal y tambi�n lleva la gesti�n del men� de comandos 
	del juego y otros eventos.
	
	Autor:
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk

	Licencia:
	GNU/GPL Versi�n 2.0
*/
import java.util.*;
import javax.microedition.midlet.*;
import javax.microedition.lcdui.*;

public class ManoloDice extends MIDlet implements CommandListener, AvisoRepintar, Aviso, AvisoRetornoDesdeDonativos, ManejadorPuntuaciones, AvisoErrorEnviandoPuntuaciones, AvisoPuntuacionesEnviadas {
	private final static int idJuegoTablaPuntuaciones = 2;

	protected static final Command cmdAceptar = new Command("Aceptar",Command.OK,1);
	protected static final Command cmdHacerDonativo = new Command("Hacer donativo",Command.ITEM,1);
	protected static final Command cmdEnviarPuntuaciones = new Command("Enviar puntuaci�n",Command.ITEM,1);
	protected static final Command cmdAcercaDe = new Command("Acerca de...",Command.ITEM,1);
	protected static final Command cmdAyuda = new Command("Ayuda",Command.ITEM,1);
	protected static final Command cmdSalirPrevio = new Command("Salir",Command.EXIT,1);
	protected static final Command cmdSalir = new Command("Salir",Command.EXIT,1);
	protected static final Command cmdContinuarDespuesDeError = new Command("Aceptar",Command.OK,1);
	protected static final Command cmdVolverAlJuego = new Command("Volver",Command.EXIT,1);
	
	protected boolean noPausa = true;

	protected Display display;

	protected PantallaManoloDice pantallaManoloDice = null;
	protected PantallaPideNombrePuntuaciones pantallaPideNombrePuntuaciones = null;
	
	protected Puntuaciones puntuaciones;
	protected int puntosUltimoJuego;

	protected void startApp() throws 
		MIDletStateChangeException
		{
		noPausa = true;
		display = Display.getDisplay(this);
		
		if (pantallaManoloDice == null) {
			try {
				puntuaciones = new Puntuaciones(
					"ManoloDice",
					5,
					new PuntuacionesOrdenadasDesc()
				);
			} catch(Exception e) {
				avisar(null,null,"Error","No se ha podido leer la tabla de puntuaciones",TIPO_AVISO_ERROR);
				return;
			}
		
			pantallaManoloDice = new PantallaManoloDice(this,this,this);
			pantallaManoloDice.addCommand(cmdHacerDonativo);
			pantallaManoloDice.addCommand(cmdEnviarPuntuaciones);
			pantallaManoloDice.addCommand(cmdAyuda);
			pantallaManoloDice.addCommand(cmdAcercaDe);
			pantallaManoloDice.addCommand(cmdSalirPrevio);
			pantallaManoloDice.setCommandListener(this);
		}
		display.setCurrent(pantallaManoloDice);
		new Thread(pantallaManoloDice).start();
	}

	public void repintar(Runnable r) {
		if (noPausa)
			display.callSerially(r);
	}
	
	public static final int TIPO_AVISO_ERROR = 0;
	public static final int TIPO_AVISO_INFORMACION = 1;
	public static final int TIPO_AVISO_SALIR = 2;
	
	protected Displayable displayableAviso;
	protected Runnable runnableAviso;
	public void avisar(Displayable d, Runnable r, String titulo, String mensaje, int tipo) {
		Alert alerta = new Alert(titulo,mensaje,null,tipo == TIPO_AVISO_ERROR ? AlertType.ERROR : AlertType.INFO);
		alerta.setTimeout(Alert.FOREVER);
		displayableAviso = d;
		runnableAviso = r;
		if (tipo == TIPO_AVISO_ERROR) {
			alerta.addCommand(cmdContinuarDespuesDeError);
			alerta.setCommandListener(this);
			display.setCurrent(alerta);
		} else 
			display.setCurrent(alerta,d);
	}
	
	public void continuarConElJuego() {
		noPausa = true;
		display.setCurrent(pantallaManoloDice);
		new Thread(pantallaManoloDice).start();
	}
	
	public void retornoDesdeDonativos() {
		continuarConElJuego();
	}
	
	public Object[][] devPuntuaciones() {
		int nFilas = puntuaciones.devNumRegistros();
		Object[][] tabla = new Object[nFilas][2];
		for (int i = 0; i < nFilas; i++) {
			tabla[i][0] = puntuaciones.devNombre(i);
			tabla[i][1] = new Integer(puntuaciones.devPuntos(i));
		}
		return tabla;
	}
	
	public void insertarPuntuacion(int puntos) {
		// Si se puede insertar la puntuaci�n, aparecer� una pantalla para 
		// introducir el nombre, si no, continuar� con el juego
		if (puntuaciones.esInsertable(puntos)) {
			puntosUltimoJuego = puntos;
			if (pantallaPideNombrePuntuaciones == null) {
				pantallaPideNombrePuntuaciones = new PantallaPideNombrePuntuaciones();
				pantallaPideNombrePuntuaciones.addCommand(cmdAceptar);
				pantallaPideNombrePuntuaciones.setCommandListener(this);
			}
			pantallaPideNombrePuntuaciones.ponPuntos(puntos);
			noPausa = false;
			display.setCurrent(pantallaPideNombrePuntuaciones);
		}
	}
	
	public void puntuacionesEnviadas() {
		continuarConElJuego();
	}
	
	public void errorEnviandoPuntuaciones(String mensaje) {
		avisar(
			pantallaManoloDice,
			pantallaManoloDice,
			"Error",
			"No se han enviado las puntuaciones\n" + mensaje, 
			TIPO_AVISO_ERROR
			);
	}
	
	public void commandAction(Command c, Displayable d) {
		if (c == cmdSalir) {
			try { 
				destroyApp(false);
				notifyDestroyed();
			} catch(MIDletStateChangeException e) {
				Alert alerta = new Alert("Error","No se ha podido finalizar la aplicacion",null,AlertType.ERROR);
				alerta.setTimeout(Alert.FOREVER);
				display.setCurrent(alerta,d);
			}
		} else if (c == cmdContinuarDespuesDeError) {
			continuarConElJuego();
		} else if (c == cmdHacerDonativo) {
			noPausa = false;
			new PantallaHacerDonativo(display,this);
		} else if (c == cmdAcercaDe) {
			PantallaMensaje pm = new PantallaMensaje(
				"Manolo dice... 1.0",
				"Esta aplicaci�n se distribuye con copyright bajos los t�rminos de la GNU General Public License (versi�n 2 o superior).\nPuedes encontrar una copia en http://www.gnu.org/licenses/gpl.html\nJos� Roberto Garc�a Chico\nwww.josergc.tk\njosergc@lycos.es\n11 de noviembre de 2006",
				null
				);
			pm.addCommand(cmdVolverAlJuego);
			pm.setCommandListener(this);
			display.setCurrent(pm);
		} else if (c == cmdSalirPrevio) {
			PantallaMensaje pm = new PantallaMensaje(
				"Manolo dice... 1.0",
				"Gracias por jugar a 'Manolo dice...'\nVisite nuestra web www.josergc.tk para conseguir m�s juegos\nJos� Roberto Garc�a Chico\njosergc@lycos.es\n11 de noviembre de 2006\n:-)",
				null
				);
			pm.addCommand(cmdSalir);
			pm.setCommandListener(this);
			display.setCurrent(pm);
		} else if (c == cmdAyuda) {
			PantallaMensaje pm = new PantallaMensaje(
				"Manolo dice... 1.0",
				"Repite las secuencias que te indique el ordenador.\nUsa las teclas del cursor para indicar qu� sector crees que es el correcto.",
				null
				);
			pm.addCommand(cmdVolverAlJuego);
			pm.setCommandListener(this);
			display.setCurrent(pm);
		} else if (c == cmdVolverAlJuego) {
			continuarConElJuego();
		} else if (c == cmdAceptar) {
			 if (d == pantallaPideNombrePuntuaciones) {
				try {
					puntuaciones.insertaNuevaPuntuacion(
						pantallaPideNombrePuntuaciones.devNombre(),
						puntosUltimoJuego,
						0
						);
					noPausa = true;
					pantallaManoloDice.empezar();
					display.setCurrent(pantallaManoloDice);
					new Thread(pantallaManoloDice).start();
				} catch (Exception e) {
					avisar(
						pantallaManoloDice,
						pantallaManoloDice,
						"Error",
						"No se han podido guardar las puntuaciones\n" + e.toString(), 
						TIPO_AVISO_ERROR
						);
				}
			 }
		} else if (c == cmdEnviarPuntuaciones) {
			noPausa = false;
			PantallaEnviarPuntuaciones pantallaEnviarPuntuaciones = new PantallaEnviarPuntuaciones(this,this,idJuegoTablaPuntuaciones,puntuaciones);
			display.setCurrent(pantallaEnviarPuntuaciones);
			new Thread(pantallaEnviarPuntuaciones).start();
		}
	}
	
	protected void pauseApp() {
		noPausa = false;
	}
	
	protected void destroyApp(boolean unconditional) throws 
		MIDletStateChangeException 
		{
		noPausa = true;
	}
	
}

