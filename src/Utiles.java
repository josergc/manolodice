/**
	Nombre del proyecto:
	Comunes

	Descripci�n de la clase:
	Algunas funciones �tiles.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.1 - 11/11/2006
		A�adidas las funciones
			public static String[] divideCadena(String cadena, int anchoMax, Font f)
			public static void pintaCadenasAlineacionIzquierda(Graphics g, String[] cadenas, int x, int y)
			public static void pintaCadenasCentradas(Graphics g, String[] cadenas, int x, int y)
			public static void pintaCadenasAlineacionDerecha(Graphics g, String[] cadenas, int x, int y)
	1.0 - 05/11/2006
*/

import java.io.*;
import java.util.*;
import javax.microedition.lcdui.*;

public class Utiles {

	// Pinta una cadena de texto a partir del punto (x,y) de arriba a abajo
	public static void pintaCadenaVerticalHaciaAbajo(Graphics g, String cadena, int x, int y) {
		Font f = g.getFont();
		int altoCaracter = f.getHeight();
		int tamCadena = cadena.length();
		char[] vectorCaracteres = cadena.toCharArray();
		for (int i = 0; i < tamCadena; i++, y += altoCaracter) {
			g.drawChar(vectorCaracteres[i],x,y,Constantes.TOPLEFT);
		}
	}
	
	// Pinta una cadena de arriba a abajo usando el punto (x,y) como centro
	public static void pintaCadenaVerticalCentrada(Graphics g, String cadena, int x, int y) {
		Font f = g.getFont();
		int altoCaracter = f.getHeight();
		int tamCadena = cadena.length();
		char[] vectorCaracteres = cadena.toCharArray();
		y -= (altoCaracter * tamCadena) >> 1;
		for (int i = 0; i < tamCadena; i++, y += altoCaracter) {
			g.drawChar(vectorCaracteres[i],x,y,Constantes.TOPLEFT);
		}
	}
	
	// Divide una cadena usando 'ch' como caracter de separaci�n
	public static String[] divideCadena(String cadena, int ch) {
		Vector v = new Vector();
		int i = cadena.indexOf(ch), iAnterior = 0;
		while (i != -1) {
			v.addElement(cadena.substring(iAnterior,i));
			iAnterior = i + 1;
			i = cadena.indexOf(ch,iAnterior);
		}
		v.addElement(cadena.substring(iAnterior));
		String[] s = new String[v.size()];
		Enumeration e = v.elements();
		i = 0;
		while (e.hasMoreElements()) 
			s[i++] = (String)e.nextElement();
		return s;
	}
	
	// Esta funci�n divide la cadena de tal forma que quepa dentro del ancho 
	// dado. Se utiliza la fuente para calcula el ancho de cada palabra.
	public static String[] divideCadena(String cadena, int anchoMax, Font f) {
		String[] palabras = divideCadena(cadena,' ');
		int anchoEspacio = f.stringWidth(" ");
		int[] anchoPalabras = new int[palabras.length];
		for (int i = 0; i < palabras.length; i++) 
			anchoPalabras[i] = f.stringWidth(palabras[i] + " ");
		// Ahora genera las frases
		Vector vectorDeFrases = new Vector();
		int ancho = 0;
		int nPalabras = 0; // N�mero de palabras en el StringBuffer
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < palabras.length; i++) {
			if (ancho + anchoPalabras[i] < anchoMax) {
				sb = sb.append(palabras[i] + " ");
				ancho += anchoPalabras[i];
				nPalabras++;
			} else {
				if (nPalabras == 0) {
					vectorDeFrases.addElement(palabras[i].trim());
					sb = new StringBuffer();
					ancho = 0;
					nPalabras = 0;
				} else {
					vectorDeFrases.addElement(sb.toString().trim());
					sb = new StringBuffer(palabras[i] + " ");
					ancho = anchoPalabras[i];
					nPalabras = 1;
				}
			}
		}
		if (ancho < anchoMax) 
			vectorDeFrases.addElement(sb.toString().trim());
		Enumeration eFrases = vectorDeFrases.elements();
		palabras = new String[vectorDeFrases.size()];
		for (int i = 0; eFrases.hasMoreElements(); i++) 
			palabras[i] = (String)eFrases.nextElement();
		return palabras;
	}
	
	// Pinta cadenas
	public static void pintaCadenasAlineacionIzquierda(Graphics g, String[] cadenas, int x, int y) {
		Font f = g.getFont();
		for (int i = 0; i < cadenas.length; i++, y += f.getHeight()) 
			g.drawString(cadenas[i],x,y,Constantes.TOPLEFT);
	}
	
	// Pinta cadenas
	public static void pintaCadenasCentradas(Graphics g, String[] cadenas, int x, int y) {
		Font f = g.getFont();
		for (int i = 0; i < cadenas.length; i++, y += f.getHeight()) 
			g.drawString(cadenas[i],x,y,Constantes.TOPHCENTER);
	}
	
	// Pinta cadenas
	public static void pintaCadenasAlineacionDerecha(Graphics g, String[] cadenas, int x, int y) {
		Font f = g.getFont();
		for (int i = 0; i < cadenas.length; i++, y += f.getHeight()) 
			g.drawString(cadenas[i],x,y,Constantes.TOPRIGHT);
	}
	
	// Copia un flujo
	public static void copiaFlujo(InputStream is, OutputStream os) throws IOException {
		int nBytesLeidos = 0;
		byte[] ba = new byte[512];
		while ((nBytesLeidos = is.read(ba)) > 0)
			os.write(ba,0,nBytesLeidos);
	}
	
	// Flujo a array de bytes
	public static byte[] flujoByteArray(InputStream is) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		copiaFlujo(is,baos);
		return baos.toByteArray();
	}
}